#version 430

#extension GL_ARB_shader_draw_parameters : enable
#extension GL_ARB_bindless_texture : enable

//стандартные матрицы для преобразования координат
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины

out vec3 normalCamSpace; //нормаль в системе координат камеры
out vec4 posCamSpace; //координаты вершины в системе координат камеры
out vec2 texCoord; //текстурные координаты

struct Material {
    uvec2 diffuseTex;
    uvec2 specularTex;
};

struct DrawDescription {
    mat4 modelMatrix;
    Material material;
};

layout(std430, binding=0) buffer materialSSBO {
    DrawDescription[] drawcalls;
};

out flat sampler2D diffuseTex;
out flat sampler2D specularTex;

void main()
{
    // gl_DrawID индексируются в рамках команды glMultiDraw*, а сам массив в общем случае может использоваться в нескольких командах.
    // Следовательно, в общем случае нужно задавать uniform-переменную - смещение в массиве.
    DrawDescription drawcall = drawcalls[gl_DrawIDARB];
    diffuseTex = sampler2D(drawcall.material.diffuseTex);
    specularTex = sampler2D(drawcall.material.specularTex);

	texCoord = vertexTexCoord;

	posCamSpace = viewMatrix * (drawcall.modelMatrix * vec4(vertexPosition, 1.0)); //преобразование координат вершины в систему координат камеры
	normalCamSpace = normalize(mat3(viewMatrix) * (mat3(drawcall.modelMatrix) * vertexNormal)); //преобразование нормали в систему координат камеры
	
	gl_Position = projectionMatrix * posCamSpace;
}
