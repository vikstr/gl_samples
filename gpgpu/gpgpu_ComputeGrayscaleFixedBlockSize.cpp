#include <Application.hpp>

#include <Texture.hpp>
#include <ShaderProgram.hpp>

/**
3 грани куба (вариант без индексов)
*/
class SampleApplication : public Application
{
public:
    //Идентификатор шейдерной программы
    ShaderProgramPtr grayscaleProgram;

    TexturePtr inputTexture;
    TexturePtr outputTexture;

    void run() override {
        // Load shader:
        grayscaleProgram = std::make_shared<ShaderProgram>();
        grayscaleProgram->createProgramCompute("gpgpushaders/grayscale_fixedblocksize.comp");

        // Load input texture:
        inputTexture = loadTexture("images/image_8k.png");

        int width, height;
        inputTexture->getSize(width, height);

        // Create output texture of the same size:
        outputTexture = std::make_shared<Texture>();
        outputTexture->initStorage2D(1, GL_RGBA8, width, height);

        grayscaleProgram->use();

        // Bind textures to binding points so that shader can access them:
        glBindImageTexture(0, inputTexture->texture(), 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
        glBindImageTexture(1, outputTexture->texture(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);

        glm::ivec2 gridSize(width, height);
        glm::ivec2 blockSize(8,4);
        glm::ivec2 blockCount = (gridSize + blockSize - 1) / blockSize;

        // Dispatch compute call for case when block size is set in shader:
        glDispatchCompute(blockCount.x, blockCount.y, 1);

        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

        outputTexture->saveRGBA8_PNG("grayscale.png");
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}