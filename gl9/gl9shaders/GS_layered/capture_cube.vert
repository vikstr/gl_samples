#version 330

layout(location = 0) in vec4 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины

out VS_OUT {
    vec3 normalWorldSpace;
    vec2 texCoord;
} vsout;

uniform mat4 M;
uniform mat3 M_normals;

void main() {
	vsout.texCoord = vertexTexCoord;
	vsout.normalWorldSpace = M_normals * vertexNormal;
	gl_Position = M * vertexPosition;
}
