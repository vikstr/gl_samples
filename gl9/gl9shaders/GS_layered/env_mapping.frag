#version 330

uniform sampler2D diffuseTex;
uniform samplerCube cubeTex;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

uniform mat3 V_inv;

void main() {
	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции

	vec3 cubemapDir = reflect(posCamSpace.xyz, normal);
	cubemapDir = V_inv * cubemapDir;
	vec4 cubemapColor = texture(cubeTex, cubemapDir);

    fragColor = cubemapColor;
}
