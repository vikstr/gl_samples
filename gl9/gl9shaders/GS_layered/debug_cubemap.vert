#version 450 core

in vec2 ndcPos; // XY - NDC [-1; 1]

out vec2 texCoord;

void main() {
    texCoord = ndcPos;
	gl_Position = vec4(ndcPos, 0, 1);
}
