#version 330

const float PI = 3.14159265359;

in vec2 texCoord;
layout (location = 0) out vec4 out_color;

uniform samplerCube cubemap;

// Two hemispheres: Z- and Z+
vec3 getDirection_hemispheres() {
    vec2 xy = vec2(2 * abs(texCoord.x) - 1, texCoord.y);
    float len2 = dot(xy,xy);
    if (len2 > 1)
        return vec3(0);
    float z = sign(texCoord.x) * sqrt(1 - len2);
    return vec3(xy,z);
}

vec3 getDirection_rect() {
    vec2 angles = texCoord * vec2(PI, PI/2);
    vec3 direction = vec3(cos(angles.x) * cos(angles.y), sin(angles.x) * cos(angles.y), sin(angles.y));
    return direction;
}

void main() {
//    vec3 direction = getDirection_rect();
    vec3 direction = getDirection_hemispheres();

    if (length(direction) < 1e-3)
        discard;

    out_color = texture(cubemap, direction);
}
